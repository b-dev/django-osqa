"""Setup script of django-osqa"""
import os
from setuptools import setup
from setuptools import find_packages

setup(
    name='django-osqa',
    version='0.1',
    description='Q&A Application for Django',
    keywords='django, blog, question, answer',

    author='Marco Minutoli',
    author_email='info@marcominutoli.it',
    #url=zinnia.__url__,

    packages=find_packages(),
    classifiers=[
        'Framework :: Django',
        'Environment :: Web Environment',
        'Programming Language :: Python',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: BSD License',
        'Topic :: Software Development :: Libraries :: Python Modules'],

    license=open('LICENSE').read(),
    install_requires=[]
)
